from ctypes import ArgumentError
from typing import List, Type, Union, Tuple

import llvmlite.ir as ll
import llvmlite.binding as llvm
from llvmlite import ir
import numba
from numba.core.registry import cpu_target

llvm.initialize()
llvm.initialize_native_target()
llvm.initialize_native_asmprinter()

context = cpu_target.target_context.with_aot_codegen('aot_codegen')
codegen = context.codegen()
library = codegen.create_library('lib')

from numba.core.compiler_lock import global_compiler_lock


module = ir.Module('mod')
double = ir.DoubleType()
fnty = ir.FunctionType(double, (double, double))
func = ir.Function(module, fnty, name="fpadd")

# Now implement the function
block = func.append_basic_block(name="entry")
builder = ir.IRBuilder(block)
a, b = func.args
result = builder.fadd(a, b, name="res")
builder.ret(result)

library.add_ir_module(module)
with global_compiler_lock:
    library.finalize()

# for fn in library.get_defined_functions():
#     fn.visibility = "default"

with open('my_lib', 'wb') as fout:
    fout.write(library.emit_native_object())

