import ctypes
from ctypes import c_char, c_int32
import llvmlite.binding as llvm
import llvmlite.ir as ll
import ctypes
import numpy as np
import numba

llvm.initialize()
llvm.initialize_native_target()
llvm.initialize_native_asmprinter()

from numba.extending import get_cython_function_address

# getting address from process and also using LD_PRELOAD
# this needs numba as far as i can tell? 
print("malloc in the process: ", llvm.address_of_symbol("malloc"))

# using ctypes
jemalloc_lib = ctypes.cdll.LoadLibrary('/Volumes/SABRENT/facebook_mbp/home/python/llvmlite_example/jemalloc-5.2.1/build/install/lib/libjemalloc.2.dylib')
jemalloc_ptr = jemalloc_lib.je_malloc
addr = ctypes.cast(jemalloc_ptr, ctypes.c_void_p).value
print("malloc from the dll: ", addr)

# using llvmlite 
llvm.load_library_permanently('/Volumes/SABRENT/facebook_mbp/home/python/llvmlite_example/jemalloc-5.2.1/build/install/lib/libjemalloc.2.dylib')
print("loaded via load_library_permanently: ", llvm.address_of_symbol("malloc"))