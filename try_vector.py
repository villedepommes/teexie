import ctypes
from ctypes import c_char, c_int32
import llvmlite.binding as llvm
import llvmlite.ir as ll
import ctypes
import numpy

llvm.initialize()
llvm.initialize_native_target()
llvm.initialize_native_asmprinter()

print("hello")
print(llvm.address_of_symbol('add'))
libadd = ctypes.cdll.LoadLibrary("/Users/villedepommes/python/llvmlite_example/build/libadd.dylib") 
llvm.load_library_permanently("/Users/villedepommes/python/llvmlite_example/build/libadd.dylib")
llvm.load_library_permanently('/Volumes/SABRENT/facebook_mbp/home/python/llvmlite_example/jemalloc-5.2.1/build/install/lib/libjemalloc.2.dylib')

# print(libadd.INT_SYMBOL)
# print(llvm.address_of_symbol('add'))
# print(llvm.address_of_symbol('INT_SYMBOL'))
# print(llvm.address_of_symbol('_INT_SYMBOL'))
print(llvm.address_of_symbol('je_malloc'))





fnty = ll.FunctionType(ll.IntType(32), [])
module = ll.Module()
func = ll.Function(module, fnty, name="broadcast")
int8x32_t = ll.VectorType(ll.IntType(32), 8)
int32_t = ll.IntType(32)
bb_entry = func.append_basic_block()
builder = ll.IRBuilder()
builder.position_at_end(bb_entry)
mask = ll.Constant(int8x32_t, [0, 0, 0, 0, 0, 0, 0, 0])
forty_two = ll.Constant(int32_t, 42)
uv = ll.Constant(int8x32_t, ll.Undefined)
v = builder.insert_element(uv, forty_two, ll.Constant(int32_t, 0))
v2 = builder.shuffle_vector(v, uv, mask)
e = builder.extract_element(v2, ll.Constant(int32_t, 7))
builder.ret(e)

strmod = str(module) 
print(strmod)

llmod = llvm.parse_assembly(strmod)
llmod.verify()
### optimize

pmb = llvm.create_pass_manager_builder()
pmb.opt_level = 2
pm = llvm.create_module_pass_manager()
pmb.populate(pm)

pm.run(llmod)
print(str(llmod))

target_machine = llvm.Target.from_default_triple().create_target_machine()
with llvm.create_mcjit_compiler(llmod, target_machine) as ee:
    ee.finalize_object()
    cfptr = ee.get_function_address("broadcast")

    print(target_machine.emit_assembly(llmod))
    cfunc = ctypes.CFUNCTYPE(c_int32)(cfptr)
    
    b = cfunc()
    print(b)
    #A = np.arange(10, dtype=np.int32)
    #res = cfunc(A.ctypes.data_as(POINTER(c_int)), A.size)
    #print(res, A.sum())