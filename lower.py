from ctypes import ArgumentError
from typing import List, Type, Union, Tuple
import llvmlite.ir as ll
import llvmlite.binding as llvm
from ir import Datatype, FloatTensor, IntTensor, Float, Int, IntVector, Add, Eq, Var, Load, Constant, Statement, \
    Store, Allocate, Block, For, If, Print, Param, Return, t2rt
from utils import SymbolTable

class LLVMLowerer:
    @staticmethod
    def get_stores(s: Statement):
        res = []
        if isinstance(s, Store):
            if s._val._datatype._size == 0:
                res.append(s)
        elif isinstance(s, Block):
            for ss in s._statements:
                res.extend(LLVMLowerer.get_stores(ss))
        
        return res
        
    def get_unique_stores(s: Statement):
        store_list = LLVMLowerer.get_stores(s)
        return set(store_list)

    def get_unique_vars(s: Statement):
        return set([x._var for x in LLVMLowerer.get_stores(s)])

    def __init__(self, stmt: Statement):
        self._symbols = SymbolTable()
        self._symbols.push_scope()
        params = stmt._statements[0]
        args = []
        for p in params._parameters:
            # TODO: support tensor
            args.append(p._datatype._datatype)

        ret_stmt = stmt._statements[-1]
        if len(ret_stmt._return_vals) > 1:
            raise ArgumentError("Expected a single return stmt")

        # support return types
        ret_type = ret_stmt._return_vals[0]._datatype._datatype if len(ret_stmt._return_vals) == 1 else ll.VoidType()
        func_type = ll.FunctionType(ret_type, args)
        self._module = ll.Module()
        malloc_type = ll.FunctionType(ll.IntType(32).as_pointer(), [ll.IntType(64)])
        self._malloc = ll.Function(self._module, malloc_type, name="je_malloc")
        pprint_type = ll.FunctionType(ll.VoidType(), [ll.IntType(32).as_pointer()])
        self._pprint = ll.Function(self._module, pprint_type, name="pprint")
        func_name = stmt._name if stmt._name else "main"
        self._func = ll.Function(self._module, func_type, func_name)
        for i, p in enumerate(params._parameters):
            if not isinstance(p, Var):
                raise ArgumentError("a parameter expected to be a variable")
            self._symbols[p] = self._func.args[i]
            self._func.args[i].name = p._name
            
        self._builder = ll.IRBuilder()
        self._block = self._func.append_basic_block()
        self._builder.position_at_end(self._block)
        # 1: to skip params
        for s in stmt._statements[1:]:
            self.visit(s)

    def visit(self, obj):
        if isinstance(obj, Param):
            raise ArgumentError("NYI")
        elif isinstance(obj, Store):
            rhs = self.visit(obj._val)
            if obj._index is None:
                self._symbols[obj._var] = rhs
            else:
                buffer = self.visit(obj._var)
                # vector type
                if (obj._val._datatype._size > 0):
                    buffer = self._builder.bitcast(buffer, t2rt(obj._val._datatype).as_pointer())
                index = self.visit(obj._index)
                ptr = self._builder.gep(buffer, [index])
                self._builder.store(rhs, ptr, align=1)
            return None
        elif isinstance(obj, Var):
            return self._symbols[obj]
        elif isinstance(obj, Load):
            buffer = self.visit(obj._buffer)
            if (obj._datatype._size > 0):
                buffer = self._builder.bitcast(buffer, t2rt(obj._datatype).as_pointer())
            index = self.visit(obj._index)
            ptr = self._builder.gep(buffer, [index])
            load = self._builder.load(ptr, align=1)
            return load
        elif isinstance(obj, Print):
            val = self.visit(obj._val)
            casted = self._builder.bitcast(val, ll.IntType(32).as_pointer())
            self._builder.call(self._pprint, [casted])
        elif isinstance(obj, Add):
            left = self.visit(obj.left)
            right = self.visit(obj.right)
            val = self._builder.add(left, right)
            return val
        elif isinstance(obj, Allocate):
            size = obj._var._datatype._size
            mem = self._builder.call(self._malloc, [ll.Constant(ll.IntType(64), size)])
            cast_mem = self._builder.bitcast(mem, obj._var._datatype._datatype)
            self._symbols[obj._var] = cast_mem
            return None
        elif isinstance(obj, Eq):
            left = self.visit(obj.left)
            right = self.visit(obj.right)
            if obj.left._datatype == Int:
                return self._builder.icmp_signed('==', left, right)
            else:
                return self._builder.fcmp_ordered('==', left, right)
        elif isinstance(obj, Constant):
            typ = obj._datatype._datatype
            if (obj._datatype._size > 0):
                typ = ll.VectorType(obj._datatype._datatype, obj._datatype._size)
            cnst = ll.Constant(typ, obj._val)
            print(f"cnst={str(cnst)}")
            self._symbols[obj] = cnst
            return self._symbols[obj]
        elif isinstance(obj, Return):
            if self._func.ftype.return_type == ll.VoidType():
                self._builder.ret_void
            else:
                self._builder.ret(self._symbols[obj._return_vals[0]])
        elif isinstance(obj, If):
            # raise NotImplementedError("NYI")
            cond = self.visit(obj._cond)
            bb_then = self._func.append_basic_block()
            bb_else = self._func.append_basic_block()
            bb_merge = self._func.append_basic_block()
            self._builder.cbranch(cond, bb_then, bb_else)

            then_vars = LLVMLowerer.get_unique_vars(obj._then)
            else_vars = LLVMLowerer.get_unique_vars(obj._else)
            if_vars = then_vars.union(else_vars)

            self._builder.position_at_end(bb_then)
            self._block = bb_then
            self._symbols.push_scope()
            self.visit(obj._then)
            then_scope = self._symbols.pop_scope()
            self._builder.branch(bb_merge)
            bb_then_final = self._block

            self._builder.position_at_end(bb_else)
            self._block = bb_else
            self._symbols.push_scope()
            self.visit(obj._else)
            else_scope = self._symbols.pop_scope()
            self._builder.branch(bb_merge)
            bb_else_final = self._block

            self._builder.position_at_end(bb_merge)
            self._block = bb_merge

            # insert phis
            for v in if_vars:

                phi = self._builder.phi(v._datatype._datatype, v._name)
                then_def = self._symbols[v]
                else_def = self._symbols[v]
                if v in then_scope:
                    then_def = then_scope[v]
                if v in else_scope:
                    else_def = else_scope[v]

                if not then_def or not else_def:
                    raise ArgumentError(f"Varible {v._name} isn't defined on one of the paths")
                
                phi.add_incoming(then_def, bb_then_final)
                phi.add_incoming(else_def, bb_else_final)
                self._symbols[v] = phi

        elif isinstance(obj, For):
            bb_loop = self._func.append_basic_block()
            start = self.visit(obj._start)
            stop = self.visit(obj._stop)
            stride = self.visit(obj._inc)
            self._builder.branch(bb_loop)
            self._builder.position_at_end(bb_loop)

            index_phi = self._builder.phi(Int._datatype, obj._var._name)
            self._symbols[obj._var] = index_phi
            index_phi.add_incoming(start, self._block)

            phis = {}
            print(LLVMLowerer.get_stores(obj))
            for s in LLVMLowerer.get_stores(obj):
                if s._var in self._symbols and s._var not in phis:
                    phi = self._builder.phi(s._var._datatype._datatype, s._var._name)
                    phi.add_incoming(self._symbols[s._var], self._block)
                    self._symbols[s._var] = phi
                    phis[s._var] = phi
            
            self._block = bb_loop
            for s in obj._statements:
                self.visit(s)

            index_inc = self._builder.add(index_phi, stride)
            index_phi.add_incoming(index_inc, self._block)

            for (v, p) in phis.items():
                p.add_incoming(self._symbols[v], self._block)

            bb_exit = self._func.append_basic_block()
            cond = self._builder.icmp_unsigned('<', index_inc, stop)
            self._builder.cbranch(cond, bb_loop, bb_exit)
            self._block = bb_exit
            self._builder.position_at_end(bb_exit)

            # builder.branch(bb_loop)
            # builder.position_at_end(bb_loop)
        elif isinstance(obj, Block):
            for s in obj._statements:
                self.visit(s)
        else:
            raise ArgumentError(f"Unexpected op type: {type(obj).__name__}")

def compile_to_llvm_mod(p: Statement):
    lowerer = LLVMLowerer(p)
    strmod = str(lowerer._module)
    print(strmod)
    llmod = llvm.parse_assembly(strmod)
    llmod.verify()
    return (llmod, lowerer)