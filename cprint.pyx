#cython: language_level=3
from libc.stdio cimport printf
from libc.stdlib cimport malloc

cdef api void pprint(void* p):
    printf("pointer's addr is %p\n", p)

cdef api void* cmalloc(size_t s):
    return malloc(s)