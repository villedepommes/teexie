from libc.math cimport exp

cdef api double myexp(double x):
    return exp(x)

