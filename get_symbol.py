import ctypes
from ctypes import c_char, c_int32
import llvmlite.binding as llvm
import llvmlite.ir as ll
import ctypes
import numpy

llvm.initialize()
llvm.initialize_native_target()
llvm.initialize_native_asmprinter()

print("hello")
print(llvm.address_of_symbol('add'))
libadd = ctypes.cdll.LoadLibrary("/Users/villedepommes/python/llvmlite_example/build/libadd.dylib") 
llvm.load_library_permanently("/Users/villedepommes/python/llvmlite_example/build/libadd.dylib")
llvm.load_library_permanently('/Volumes/SABRENT/facebook_mbp/home/python/llvmlite_example/jemalloc-5.2.1/build/install/lib/libjemalloc.2.dylib')

# print(libadd.INT_SYMBOL)
# print(llvm.address_of_symbol('add'))
# print(llvm.address_of_symbol('INT_SYMBOL'))
# print(llvm.address_of_symbol('_INT_SYMBOL'))
print(llvm.address_of_symbol('je_malloc'))

fnty = ll.FunctionType(ll.IntType(32).as_pointer(), [])
module = ll.Module()

malloc_type = ll.FunctionType(ll.IntType(32).as_pointer(), [ll.IntType(64)])

func = ll.Function(module, fnty, name="use_malloc")
malloc = ll.Function(module, malloc_type, name="je_malloc")
bb_entry = func.append_basic_block()
builder = ll.IRBuilder()
builder.position_at_end(bb_entry)
mem = builder.call(malloc, [ll.Constant(ll.IntType(64), 64)])
ptr = builder.gep(mem, [ll.Constant(ll.IntType(32), 0)])
ptr_2 = builder.gep(mem, [ll.Constant(ll.IntType(32), 1)])
value = builder.store(ll.Constant(ll.IntType(32), 64), ptr)
value = builder.store(ll.Constant(ll.IntType(32), 63), ptr_2)
builder.ret(mem)


strmod = str(module) 
print(strmod)

llmod = llvm.parse_assembly(strmod)
llmod.verify()
### optimize

pmb = llvm.create_pass_manager_builder()
pmb.opt_level = 2
pm = llvm.create_module_pass_manager()
pmb.populate(pm)

pm.run(llmod)
print(str(llmod))

target_machine = llvm.Target.from_default_triple().create_target_machine()
with llvm.create_mcjit_compiler(llmod, target_machine) as ee:
    ee.finalize_object()
    cfptr = ee.get_function_address("use_malloc")

    print(target_machine.emit_assembly(llmod))
    cfunc = ctypes.CFUNCTYPE(ctypes.POINTER(c_int32))(cfptr)
    
    b = cfunc()
    # more here https://stackoverflow.com/a/63030118
    arr = numpy.ctypeslib.as_array(b, shape=(16,))
    print(arr)
    print(str(b))
    #A = np.arange(10, dtype=np.int32)
    #res = cfunc(A.ctypes.data_as(POINTER(c_int)), A.size)
    #print(res, A.sum())