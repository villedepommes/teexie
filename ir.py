from ctypes import ArgumentError
from typing import List, Type, Union, Tuple
import llvmlite.ir as ll
import llvmlite.binding as llvm
#local imports
from utils import NoParent, get_id

parent = None
INDENT = 4
depth = 1

class Datatype:
    def __init__(self, datatype: llvm.ir.Type, size : int):
        self._datatype = datatype
        self._size = size

    def __call__(self, val):
        if isinstance(val, str):
            return Var(self, val)
        else:
            if self._size != 0:
                raise ArgumentError("Only scalar constants are supported")
            return Constant(self, val)

    def __str__(self):
        return f"{str(self._datatype)}({self._size})"

    def __eq__(self, other):
        return self._datatype == other._datatype and \
            self._size == other._size

def t2mt(typ: Datatype):
    return typ._datatype if typ._size == 0 else typ._datatype.as_pointer()

def IntTensor(size: int):
        if size == 0:
            type = llvm.ir.IntType(32)
        else:
            type = llvm.ir.IntType(32).as_pointer()
        return Datatype(type, size)

def FloatTensor(size: int):
        if size == 0:
            type = llvm.ir.FloatType()
        else:
            type = llvm.ir.FloatType().as_pointer()
        return Datatype(type, size)
        
# DESIGN NOTES: we need a way to differentiate types: between aggregated referential types (memory)
# and value types (vector registers)
def IntVector(size: int):
        if size < 2:
            raise ArgumentError("Size cannot be less than 2")
        type = llvm.ir.IntType(32)
        return Datatype(type, size)

Int = IntTensor(0)
Float = FloatTensor(0)

def t2rt(typ: Datatype):
    return typ._datatype if typ._size == 0 else ll.VectorType(typ._datatype, typ._size)

class Value:
    def __init__(self, datatype: Datatype):
        self._datatype = datatype

    def __add__(self, other):
        if self._datatype != other._datatype:
            raise ArgumentError(f"self's dtype {self._datatype} is different from {other._datatype}")
        return Add(self, other, self._datatype)

    def __eq__(self, other):
        global parent
        if parent:
            if self._datatype != other._datatype:
                raise ArgumentError(f"self's dtype {self._datatype} is different from {other._datatype}")        
            return Eq(self, other, self._datatype)
        
        return id(self) == id(other)
        #return id(self) == id(other)

    def __hash__(self):
        return (hash(id))

    def __ilshift__(self, value):
        if isinstance(self, Var):
            Store(self, value)
            return self
        else:
            raise AttributeError("Can only assign to Variable")

class Op(Value):
    def __init__(self, children : List[Value], datatype: Datatype):
        super().__init__(datatype)
        self._children = children

# view
class BinaryOp(Op):
    def __init__(self, left: Value, right: Value, datatype: Datatype):
        super().__init__([left, right], datatype)

    @property
    def left(self):
        return self._children[0]

    @property
    def right(self):
        return self._children[1]

class Add(BinaryOp):    
    def __str__(self):
        return f"{self.left} + {self.right}"

class Eq(BinaryOp):    
    def __str__(self):
        return f"{self.left} == {self.right}"

class Var(Value):
    def __init__(self, datatype: Datatype, name: str):
        super().__init__(datatype)
        self._id = get_id()
        self._name = name
    
    # def __hash__(self):
    #     return hash(self._id)
    def __str__(self):
        return f"{self._name}_{self._id}"

    def __getitem__(self, key):
        if isinstance(key, Var):
            return Load(self, key, Datatype(self._datatype._datatype.pointee, 0))
        elif isinstance(key, slice):
            if not isinstance(key.stop, int):
                raise ArgumentError("Number of elements expected to be an integer")
            return Load(self, key.start, Datatype(self._datatype._datatype.pointee, key.stop))

    def __setitem__(self, key: Union[Value,Tuple[Value, Datatype]], val : Value):
        return Store(self, val, key)

class Load(Value):
    def __init__(self, buffer: Var, index: Value, datatype: Datatype, name: str = ""):
        super().__init__(datatype)
        self._name = name
        self._buffer = buffer
        self._index = index
        self._datatype = datatype

        if buffer._datatype._size < 1:
            raise ArgumentError(f"Cannot load from a scalar {buffer._name}")

    def __str__(self):
        return f"{self._buffer._name}[{str(self._index)}]"

class Constant(Value):
    def __init__(self, datatype: Datatype, val: Union[str, List[Union[int, float]]]):
        super().__init__(datatype)
        self._val = val

    def __str__(self):
        return str(self._val)

class Statement:
    def __init__(self, name : str):
        self._parent = parent
        self._name = name
        if parent:
            # print("adding " + type(self).__name__ + " to " + parent._name)
            parent._statements.append(self)
        
    def __enter__(self):
        global parent
        self._parent = parent
        parent = self
        print ("Entering " + self._name)
        return self

    def __exit__(self, exception_type, exception_val, trace):
        print ("Exiting " + self._name)
        global parent
        print("exc = ", str(exception_val))
        import traceback
        traceback.print_tb(trace)
        parent = self._parent
        return self

class Store(Statement):
    def __init__(self, var: Var, val: Value, index: Value = None, name: str = ""):
        super().__init__(name)
        self._var = var
        self._val = val
        self._index = index

    def __str__(self):
        if self._val._datatype._size == 0:
            return f"{str(self._var)} = {str(self._val)}"
        else:
            return f"{self._var._name}[{str(self._index)}] = {str(self._val)}"

class Allocate(Statement):
    def __init__(self, var: Var):
        super().__init__(var._datatype)
        self._var = var

    def __str__(self):
        return f"Allocate({self._var})"

class Block(Statement):
    def __init__(self, name : str = ""):
        super().__init__(name)
        self._statements = []

    def _depth(self):
        depth = 0
        it = self
        while it != None:
            depth += 1
            it = self._parent

        return depth

    def __str__(self):
        #depth = self._depth()
        block_str = self.header()
        for s in self._statements:
            indent = "".join([' '] * INDENT)
            indent_stmt = indent + str(s).replace('\n', '\n' + indent)
            block_str +=  indent_stmt.rstrip() + '\n'

        return block_str

    def header(self):
        name = "" if self._name == "" else self._name
        return name + "\n"

class For(Block):
    def __init__(self, start: Value, stop: Value, inc: Value, name : str = ""):
        print("Entering For")
        super().__init__(name)
        self._start = start
        self._stop = stop
        self._inc = inc
        self._var = Int("for")

    def header(self):
        return f"For (start : {self._start}, stop : {self._stop}, increment : {self._inc}):\n"

class If(Statement):
    def __init__(self, cond : Value, name : str = ""):
        super().__init__(name)
        with NoParent():
            self._then = Block("then1")
            self._else = Block("else1")
        self._cond = cond

    @property
    def Then(self):
        return self._then

    @property
    def Else(self):
        return self._else

    def __str__(self):
        if_str = f"if {self._cond} then:\n"
        if_str += str(self._then)
        if_str += f"else:"
        if_str += str(self._else)
        return if_str

class Param(Statement):
    def __init__(self, parameters : List[Value]):
        super().__init__("")
        self._parameters = parameters

    def __str__(self):
        return f"Param({', '.join(map(lambda x: str(x), self._parameters))})"

class Print(Statement):
    def __init__(self, val: Value):
        super().__init__("")
        self._val = val

    def __str__(self):
        return f"print({self._val})"

class Return(Statement):
    def __init__(self, return_vals : List[Value]):
        super().__init__("")
        self._return_vals = return_vals

    def __str__(self):
        return f"return ({', '.join(map(lambda x: str(x), self._return_vals))})"
