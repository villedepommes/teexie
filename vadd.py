from ir import Datatype, FloatTensor, IntTensor, Float, Int, IntVector, Add, Eq, Var, Load, Constant, Statement, \
    Store, Allocate, Block, For, If, Print, Param, Return, t2rt
from utils import SymbolTable

with Block("sum") as p2:
    IntTensor64 = IntTensor(64)
    IntVec8 = IntVector(8)
    a = IntTensor64("a")
    print(type(a).__name__)
    Param([a])
    c = Int("c")
    c <<= Int(0)
    a_copy = IntTensor64("a_copy")
    Allocate(a_copy)
    Print(a_copy)
    with For(Int(0), Int(8), Int(1)) as l:
        load = a[l._var:8]
        print("load=", load._datatype)
        cnst = Constant(IntVec8, [2, 2, 2, 2, 2, 2, 2, 2])
        print("cnst=", cnst._datatype)
        a_copy[l._var] =  a[l._var:8] + cnst
    Return([a_copy])
