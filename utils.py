

symbol_count = 0

def get_id():

    global symbol_count
    id = symbol_count
    symbol_count += 1
    return id

class NoParent:
    def __init__(self):
        global parent
        self._parent = parent
    def __enter__(self):
        global parent
        parent = None
    def __exit__(self, exception_type, exception_val, trace):
        global parent
        parent = self._parent

class SymbolTable:
    def __init__(self):
        self._sym_tables = []

    def push_scope(self):
        self._sym_tables.append({})

    def pop_scope(self):
        return self._sym_tables.pop()

    def __contains__(self, key):
        return self[key] is not None

    def __getitem__(self, key):
        for st in reversed(self._sym_tables):
            if key in st:
                return st[key]
        return None

    def __setitem__(self, key, value):
        self._sym_tables[-1][key] = value