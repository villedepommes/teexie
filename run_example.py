import re

PREFIX = "_Z"

_re_invalid_char = re.compile(r'[^a-z0-9_]', re.I)

def _fix_lead_digit(text):
    """
    Fix text with leading digit
    """
    if text and text[0].isdigit():
        return '_' + text
    else:
        return text

def _escape_string(text):
    """Escape the given string so that it only contains ASCII characters
    of [a-zA-Z0-9_$].
    The dollar symbol ($) and other invalid characters are escaped into
    the string sequence of "$xx" where "xx" is the hex codepoint of the char.
    Multibyte characters are encoded into utf8 and converted into the above
    hex format.
    """
    def repl(m):
        return ''.join(('$%02x' % ch)
                       for ch in m.group(0).encode('utf8'))
    ret = re.sub(_re_invalid_char, repl, text)
    # Return str if we got a unicode (for py2)
    if not isinstance(ret, str):
        return ret.encode('ascii')
    return ret

def _len_encoded(string):
    """
    Prefix string with digit indicating the length.
    Add underscore if string is prefixed with digits.
    """
    string = _fix_lead_digit(string)
    return '%u%s' % (len(string), string)

def mangle_identifier(ident, template_params=''):
    """
    Mangle the identifier with optional template parameters.
    Note:
    This treats '.' as '::' in C++.
    """
    parts = [_len_encoded(_escape_string(x)) for x in ident.split('.')]
    if len(parts) > 1:
        return 'N%s%sE' % (''.join(parts), template_params)
    else:
        return '%s%s' % (parts[0], template_params)

# C names to mangled type code
C2CODE = {
    'void': 'v',
    'wchar_t': 'w',
    'bool': 'b',
    'char': 'c',
    'signed char': 'a',
    'unsigned char': 'h',
    'short': 's',
    'unsigned short': 't',
    'int': 'i',
    'unsigned int': 'j',
    'long': 'l',
    'unsigned long': 'm',
    'long long': 'x', # __int64
    'unsigned long long': 'y', # unsigned __int64
    '__int128': 'n',
    'unsigned __int128': 'o',
    'float': 'f',
    'double': 'd',
    'long double': 'e', # __float80
    '__float128': 'g',
    'ellipsis': 'z',
}

def mangle_type_c(typ):
    """
    Mangle C type name
    Args
    ----
    typ: str
        C type name
    """
    if typ in C2CODE:
        return C2CODE[typ]
    else:
        return mangle_identifier(typ)

def mangle_args_c(argtys):
    """
    Mangle sequence of C type names
    """
    return ''.join([mangle_type_c(t) for t in argtys])

def mangle_c(ident, argtys):
    """
    Mangle identifier with C type names
    """
    return PREFIX + mangle_identifier(ident) + mangle_args_c(argtys)

from llvmlite import ir

# Create some useful types
double = ir.DoubleType()
fnty = ir.FunctionType(double, (double, double))

# Create an empty module...
module = ir.Module(name=__file__)
# and declare a function named "fpadd" inside it
func = ir.Function(module, fnty, name="fpadd")

# Now implement the function
block = func.append_basic_block(name="entry")
builder = ir.IRBuilder(block)
a, b = func.args
result = builder.fadd(a, b, name="res")
builder.ret(result)

# Print the module IR
print(module)

