
#### LoopNest level

Value :> (Scalar, Buffer, Vector, Expr?)
Expr.children = Value
Variable.pointee = Value


Scalar -> reg
Buffer -> void*
Vector -> xmm reg
Expr -> x86 instruction

