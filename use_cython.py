import ctypes
from numba.extending import get_cython_function_address
#import pyximport; pyximport.install()

#import cprint

# functype = ctypes.CFUNCTYPE(None, ctypes.c_void_p)
# pprint = functype(cprint.pprint)
# pprint(ctypes.c_void_p(100))

#addr = get_cython_function_address("foo", "myexp")
addr = get_cython_function_address("cprint", "pprint")
functype = ctypes.CFUNCTYPE(None, ctypes.c_void_p)

print("Done")
