from __future__ import print_function

from llvmlite.binding.module import ModuleRef
from lower import compile_to_llvm_mod
from typing import List, Type, Union, Tuple
import ctypes
from ctypes import ArgumentError, CFUNCTYPE, c_float, c_int, POINTER, c_void_p, cdll
from ir import Datatype, FloatTensor, IntTensor, Float, Int, IntVector, Add, Eq, Var, Load, Constant, Statement, \
    Store, Allocate, Block, For, If, Print, Param, Return, t2rt
from utils import SymbolTable

import sys
from llvmlite.ir.types import VoidType
from numpy.lib.arraysetops import isin
try:
    from time import perf_counter as time
except ImportError:
    from time import time

import numpy as np
from numba.extending import get_cython_function_address


try:
    import faulthandler; faulthandler.enable()
except ImportError:
    pass

import llvmlite.ir as ll
import llvmlite.binding as llvm


llvm.initialize()
llvm.initialize_native_target()
llvm.initialize_native_asmprinter()
#llvm.load_library_permanently('/Volumes/SABRENT/facebook_mbp/home/python/llvmlite_example/jemalloc-5.2.1/build/install/lib/libjemalloc.2.dylib')

# jemalloc_lib = cdll.LoadLibrary('/Volumes/SABRENT/facebook_mbp/home/python/llvmlite_example/jemalloc-5.2.1/build/install/lib/libjemalloc.2.dylib')
# jemalloc_ptr = jemalloc_lib.je_malloc
# addr = ctypes.cast(jemalloc_ptr, ctypes.c_void_p).value

print("malloc", llvm.address_of_symbol("malloc"))
#addr = get_cython_function_address("cprint", "cmalloc")
addr = llvm.address_of_symbol("malloc")
llvm.add_symbol("je_malloc", addr)
free = CFUNCTYPE(None, c_void_p)(llvm.address_of_symbol("free"))

addr = get_cython_function_address("cprint", "pprint")
llvm.add_symbol("pprint", addr)
# functype = ctypes.CFUNCTYPE(None, ctypes.c_void_p)

# free_ptr = jemalloc_lib.je_free
# addr = ctypes.cast(jemalloc_ptr, ctypes.c_void_p).value
# free = CFUNCTYPE(None, c_void_p)(addr)

# with Block("program") as p:
#     a = Int("a")
#     b = Int("b")
#     Param([a, b])
#     c = Int("c")
#     c <<= Int(0)
#     with For(Int(0), Int(10), Int(1)) as l:
#         c <<= c + l._var
#     Return([c])

# with Block("program") as p:
#     a = Int("a")
#     b = Int("b")
#     Param([a, b])
#     c = Int("c")
#     d = Int("d")
#     c <<= Int(0)
#     d <<= Int(0)
#     with For(a, b, Int(1)) as l:
#         c <<= c + l._var
#         with For(l._var, b, Int(1)) as l2:
#             c <<= l2._var
#         d <<= d + Int(3)
#     Return([c])

# with Block("program") as p:
#     a = Int("a")
#     b = Int("b")
#     Param([a, b])
#     c = Int("c")
#     if1 = If(a == b)
#     with if1.Then:
#         c <<= a
#     with if1.Else:
#         c <<= b
#     Return([c])




def load_program(s: str):
    program_str = open(s).read()
    globals_ = locals()
    exec(program_str, globals_)
    blocks = [b for (n, b)  in globals_.items() if type(b) == Block]
    if (len(blocks) > 1):
        raise AttributeError("More than one program defined in a source file!")
    print(len(blocks))
    return blocks[0]

p = load_program('vadd.py')
print(p)
llmod, lowerer = compile_to_llvm_mod(p)

# with Block("sum") as p:
#     a = Float("a")
#     b = Float("b")
#     Param([a, b])
#     c = Float("c")
#     if1 = If(a == b)
#     with if1.Then:
#         c <<= a
#     with if1.Else:
#         c <<= b
#     Return([c])




def dt2ct(dtype):
    if dtype == llvm.ir.IntType(32):
        return c_int
    elif dtype == llvm.ir.FloatType:
        return c_float
    elif dtype == llvm.ir.IntType(32).as_pointer():
        return POINTER(c_int)
    elif dtype == llvm.ir.FloatType(32).as_pointer():
        return POINTER(c_float)
    elif dtype == llvm.ir.VoidType():
        return None

#  clang++-10 --print-supported-cpus this prints the list of cpus:
# llvm -mcpu=skylake-avx512
"""
        silvermont
        skx
        skylake
        skylake-avx512
        slm
"""
target_machine = llvm.Target.from_default_triple().create_target_machine(cpu='skylake', features='-avx2')


print(llvm.get_host_cpu_name())
print(llvm.get_host_cpu_features())

with llvm.create_mcjit_compiler(llmod, target_machine) as ee:
    ee.finalize_object()
    cfptr = ee.get_function_address(p._name)
    print(target_machine.emit_assembly(llmod))
    c_type_ret_type = dt2ct(lowerer._func.ftype.return_type)
    c_type_args = [dt2ct(a) for a in lowerer._func.ftype.args]
    cfunc = CFUNCTYPE(c_type_ret_type, *c_type_args)(cfptr)
    print(cfunc.argtypes)
    print(cfunc.restype)

    
    #integers = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], dtype=np.int32)
    integers = np.arange(0, 64, 1, dtype=np.int32)
    ptr_to_integers = integers.ctypes.data_as(POINTER(c_int))
    res = cfunc(ptr_to_integers)
    print(hex(ctypes.cast(res, ctypes.c_void_p).value))
    arr = np.ctypeslib.as_array(res, shape=(64,))
    print(arr)
    #free(res)
    print('Done!')
