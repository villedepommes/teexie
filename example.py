define i32 @"program"(i32 %"a", i32 %"b")
{
.4:
  %".5" = icmp eq i32 %"a", %"b"
  br i1 %".5", label %".6", label %".7"
.6:
  br label %".8"
.7:
  br label %".8"
.8:
  %"c" = phi i32 [%"a", %".6"], [%"b", %".7"]
  ret
}
